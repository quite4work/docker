import YAML from "npm:yaml";
import { delay } from "https://deno.land/std@0.148.0/async/delay.ts";
import * as base64 from "https://deno.land/std@0.178.0/encoding/base64.ts";

export async function start(
  plan: string,
  region: string,
  ignition: Record<string, unknown>,
) {
  const host = await getDockerHost();
  if (host) {
    console.log("already started");
    console.log(`export DOCKER_HOST=ssh://root@${host.main_ip}`);
    return;
  }
  console.log("creating docker host...");
  const { id } = await createDockerHost(plan, region, ignition);
  let ip;
  while (true) {
    await delay(1000);
    console.log(`waiting docker host to boot`);
    const { main_ip } = await getHost(id);
    if (main_ip && main_ip !== "0.0.0.0") {
      ip = main_ip;
      break;
    }
  }
  await removeFromKnownHosts(ip);
  await waitAddToKnownHosts(ip);
  await sshWaitReady("root", ip);
  console.log("started");
  console.log(`export DOCKER_HOST=ssh://root@${ip}`);
}

export async function stop() {
  const host = await getDockerHost();
  if (!host) {
    console.log("already stopped");
    return;
  }
  const headers = await getAuthHeaders();
  const resp = await fetch(`https://api.vultr.com/v2/instances/${host.id}`, {
    method: "DELETE",
    headers,
  });
  if (!resp.ok) {
    console.log(`bad resp: ${resp}`);
    console.log(await resp.text());
    return;
  }
  console.log("stopping...");
}

export async function reinstall() {
  const host = await getDockerHost();
  if (!host) {
    console.log("host is not running");
    return;
  }
  const headers = await getAuthHeaders();
  console.log("reinstalling...");
  const resp = await fetch(
    `https://api.vultr.com/v2/instances/${host.id}/reinstall`,
    {
      method: "POST",
      headers,
    },
  );
  console.log("starting...");
  if (!resp.ok) {
    console.log(`bad resp: ${resp}`);
    console.log(await resp.text());
    Deno.exit(1);
  }
  await removeFromKnownHosts(host.main_ip);
  await waitAddToKnownHosts(host.main_ip);
  await sshWaitReady("root", host.main_ip);
  console.log("host is ready");
}

export async function host() {
  const host = await getDockerHost();
  if (host) {
    console.log(`ssh://root@${host.main_ip}`);
  }
}

async function getAuthHeaders() {
  const home = Deno.env.get("HOME");
  const config = await Deno.readTextFile(`${home}/.vultr-cli.yaml`);
  const { "api-key": apiKey } = YAML.parse(config);
  return { Authorization: `Bearer ${apiKey}` };
}

async function getHost(id: string) {
  const headers = await getAuthHeaders();
  const resp = await fetch(`https://api.vultr.com/v2/instances/${id}`, {
    headers,
  });
  if (!resp.ok) {
    console.log(`bad resp: ${resp}`);
    console.log(await resp.text());
    Deno.exit(1);
  }
  const { instance } = await resp.json();
  return instance;
}

async function getDockerHost() {
  const headers = await getAuthHeaders();
  const resp = await fetch("https://api.vultr.com/v2/instances", { headers });
  if (!resp.ok) {
    console.log(`bad resp: ${resp}`);
    console.log(await resp.text());
    Deno.exit(1);
  }
  const { instances } = await resp.json();
  return instances.find(({ label }: { label: string }) => label === "docker");
}

async function waitAddToKnownHosts(ip: string) {
  while (!await addToKnownHosts(ip)) {
    console.log(`waiting ${ip} to add to known_hosts`);
    await delay(1000);
  }
}

async function addToKnownHosts(ip: string): Promise<boolean> {
  const home = Deno.env.get("HOME");
  const cmd = `ssh-keyscan -t ecdsa ${ip} >> ${home}/.ssh/known_hosts`;
  const { success } = await new Deno.Command("sh", { args: ["-c", cmd] })
    .output();
  return success;
}

async function removeFromKnownHosts(ip: string) {
  await new Deno.Command("ssh-keygen", { args: ["-R", ip] }).output();
}

async function sshWaitReady(user: string, host: string) {
  while (!await sshReady(user, host)) {
    console.log(`waiting ssh for ${host}`);
    await delay(1000);
  }
}

async function sshReady(user: string, host: string) {
  const { stdout } = await new Deno.Command("ssh", {
    args: [`${user}@${host}`, "whoami"],
  }).output();
  const res = new TextDecoder().decode(stdout);
  if (res.trim() !== user) {
    return false;
  }
  return true;
}

async function createDockerHost(
  plan: string,
  region: string,
  ignition: Record<string, unknown>,
) {
  const headers = await getAuthHeaders();
  const user_data = base64.encode(
    new TextEncoder().encode(JSON.stringify(ignition)),
  );
  const body = {
    region,
    plan,
    label: "docker",
    os_id: 391, // Fedora CoreOS Stable
    user_data,
  };
  const resp = await fetch("https://api.vultr.com/v2/instances", {
    method: "POST",
    headers: { ...headers, "Content-Type": "application/json" },
    body: JSON.stringify(body),
  });
  if (!resp.ok) {
    console.log(`bad resp: ${resp}`);
    console.log(await resp.text());
    Deno.exit(1);
  }
  const { instance } = await resp.json();
  return instance;
}
